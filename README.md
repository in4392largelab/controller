# in4392
### To the project started:   
1) Change azureauth.properties file in config folder.   
2) Run "mvn install" in root.
3) Run application from IDE  
or run "mvn compile exec:java".  

If you choose to use your own credentials you need to have:  
 - a resource group named "VMImageResourceGroup" which contains image of VM called "myImage".  
    
 If you choose to use Azure load balancer then you need to have:  
 - a resource group name "myResourceGroup" which contains load balancer called "myLoadBalancer".  
 
If you wish to do it all in your own account:  
- Tutorial how to get Apache and PHP code working in Apache_PHP_instructions.txt.  
- Tutorial how to create VM Image: https://docs.microsoft.com/en-us/azure/virtual-machines/linux/capture-image  
- Tutorial how to create a load balancer: https://www.youtube.com/watch?v=JjJ2ZAfO_uw  
 
  
Right now cotroller will create VM-s and Load Balancer in one resource group.  
- The downside is that you can't delete Virtual Disks with Java.  
- Upside is that this way it is possible to use load balancer (LoadBalancer requires VM-s
to be in the same resource group).
