<!DOCTYPE html>
<html>
<head>
        <title>Video Convert</title>
</head>
<body>
	Dummy video converter.
<body>
</html>
<?php
if (file_exists("./input.avi")) {
	$videoFilePath="./input.avi";

	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < 10; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
	$outputFile = "./".$randomString.".mp4";

	$cmd = "/usr/bin/ffmpeg -i ".$videoFilePath." -an ".$outputFile;
	exec($cmd." 2>&1", $out, $ret);
	if ($ret){
			echo "There was a problem!\n";
			print_r($out);
	} else{
		echo "Everything went better than expected!\n";
		if (file_exists($outputFile)) {
			unlink($outputFile);
			echo "Deleted output file";
		} else {
			echo "Output file not found";
		}
	}
}
?>
