package com.in4392;

public class App {

    /**
     *
     * @param args args[0]: Monitor app private IP; args[1]: Monitor app port;
     */
    public static void main(String[] args) {

        Controller controller = new Controller();
        //controller.createVM("rootVM");
        try {
            //controller.createVM("rootVM");
            //controller.createVM("VM2");
            //controller.createVM("VM3");
        } catch (com.microsoft.azure.CloudException e) {
            System.out.println(e.getMessage());
        }
        //Take ip address, enter it to Chrome and you will get to the PHP page
        System.out.println(controller.getIPAddresses());
        System.out.println(controller.getNames());

        Logger logger = new Logger(args[0], args[1]);
        logger.start();

        controller.controlVirtualMachines(args[0], args[1]);
    }
}
