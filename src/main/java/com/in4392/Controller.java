package com.in4392;

import com.microsoft.azure.management.Azure;
import com.microsoft.azure.management.compute.*;
import com.microsoft.azure.management.network.*;
import com.microsoft.azure.management.resources.fluentcore.arm.Region;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Controller {

    private Azure azure;

    private final String RESOURCE_GROUP = "myResourceGroup";
    private final String SUBNET_NAME = "mySubnet";
    private final String VM_IMAGE_NAME = "myImage";
    private final String VM_IMAGE_RESOURCE_GROUP_NAME = "VMImageResourceGroup";
    Long lastActionEpoch = System.currentTimeMillis();

    public Controller() {
        File authFile = new File("config/azureauth.properties");
        System.out.println("Configuration file exists: " + authFile.exists());
        try {
            System.out.println("Authenticating...");
            azure = Azure.authenticate(authFile).withDefaultSubscription();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void createVM(String VMName) {
        String resourceGroupName = RESOURCE_GROUP;

        System.out.println("Creating availability set and resource group or using existing one...");
        AvailabilitySet availabilitySet = getAvailabilitySet();

        System.out.println("Creating security group or using existing one...");
        NetworkSecurityGroup networkSecurityGroup = getNetworkSegurityGroup();

        System.out.println("Creating virtual network or using existing one...");
        Network network = getNetwork(networkSecurityGroup);

        System.out.println("Creating public IP address for: " + VMName + "...");
        PublicIPAddress publicIPAddress = azure.publicIPAddresses()
                .define(VMName)
                .withRegion(Region.EUROPE_WEST)
                .withExistingResourceGroup(resourceGroupName)
                .withDynamicIP()
                .create();

        System.out.println("Creating network interface for: " + VMName + "...");
        NetworkInterface networkInterface = azure.networkInterfaces()
                .define(VMName)
                .withRegion(Region.EUROPE_WEST)
                .withExistingResourceGroup(resourceGroupName)
                .withExistingPrimaryNetwork(network)
                .withSubnet(SUBNET_NAME)
                .withPrimaryPrivateIPAddressDynamic()
                .withExistingPrimaryPublicIPAddress(publicIPAddress)
                .create();

        VirtualMachineCustomImage customImage = azure.virtualMachineCustomImages().getByResourceGroup(VM_IMAGE_RESOURCE_GROUP_NAME, VM_IMAGE_NAME);

        System.out.println("Creating virtual machine with name "  + VMName + "...");
        VirtualMachine virtualMachine = azure.virtualMachines()
                .define(VMName)
                .withRegion(Region.EUROPE_WEST)
                .withExistingResourceGroup(resourceGroupName)
                .withExistingPrimaryNetworkInterface(networkInterface)
                .withLinuxCustomImage(customImage.id())
                .withRootUsername("in4392")
                .withRootPassword("Th1s1sPassword")
                .withComputerName(VMName)
                .withExistingAvailabilitySet(availabilitySet)
                .withSize("Standard_B1ms")
                .create();
        System.out.println("Virtual machine successfully created!");
        System.out.println("VM IP: " + virtualMachine.getPrimaryPublicIPAddress().ipAddress());

        //addVMtoLoadBalancer(VMName, networkInterface);
    }

    private void addVMtoLoadBalancer(String VMName, NetworkInterface networkInterface) {
        System.out.println("Connecting VM " + VMName + " to load balancer.");
        //get existing lb by resource group name and lb name
        System.out.println("Fetch load balancer");
        LoadBalancer loadBalancer = azure.loadBalancers().getByResourceGroup(RESOURCE_GROUP,"myLoadBalancer");

        //attach a backend for your lb
        System.out.println("Creating backend or using existing one for VM: " + VMName);
        loadBalancer.update().defineBackend("backendPool").attach().apply();

        //update nic by with lb and lb backend
        System.out.println("Updating network interface for VM: " + VMName);
        networkInterface.update().withExistingLoadBalancerBackend(loadBalancer, "backendPool").apply();
    }

    private Network getNetwork(NetworkSecurityGroup networkSecurityGroup) {
        if (azure.networkInterfaces().getByResourceGroup(RESOURCE_GROUP, "myNetworkInterface") != null) {
            return azure.networks().getByResourceGroup(RESOURCE_GROUP, "myNetworkInterface");
        }

        return azure.networks()
                .define("myNetworkInterface")
                .withRegion(Region.EUROPE_WEST)
                .withExistingResourceGroup(RESOURCE_GROUP)
                .withAddressSpace("10.0.0.0/16")
                .defineSubnet(SUBNET_NAME).withAddressPrefix("10.0.0.0/24")
                .withExistingNetworkSecurityGroup(networkSecurityGroup)
                .attach()
                .create();
    }

    private NetworkSecurityGroup getNetworkSegurityGroup() {
        NetworkSecurityGroup mySecurityGroup = azure.networkSecurityGroups()
                .define("mySecurityGroup")
                .withRegion(Region.EUROPE_WEST)
                .withExistingResourceGroup(RESOURCE_GROUP)
//                .defineRule("ALLOW-SSH")
//                .allowInbound()
//                .fromAnyAddress()
//                .fromAnyPort()
//                .toAnyAddress()
//                .toPort(22)
//                .withProtocol(SecurityRuleProtocol.TCP)
//                .withPriority(100)
//                .withDescription("Allow SSH")
//                .attach()
//                .defineRule("ALLOW-HTTP")
//                .allowInbound()
//                .fromAnyAddress()
//                .fromAnyPort()
//                .toAnyAddress()
//                .toPort(80)
//                .withProtocol(SecurityRuleProtocol.TCP)
//                .withPriority(101)
//                .withDescription("Allow HTTP")
//                .attach()
//                .defineRule("ALLOW-8080")
//                .allowInbound()
//                .fromAnyAddress()
//                .fromAnyPort()
//                .toAnyAddress()
//                .toPort(8080)
//                .withProtocol(SecurityRuleProtocol.TCP)
//                .withPriority(102)
//                .withDescription("Allow 8081")
//                .attach()
//                .defineRule("ALLOW-8081")
//                .allowInbound()
//                .fromAnyAddress()
//                .fromAnyPort()
//                .toAnyAddress()
//                .toPort(8081)
//                .withProtocol(SecurityRuleProtocol.TCP)
//                .withPriority(103)
//                .withDescription("Allow 8081")
//                .attach()
                .create();

        return mySecurityGroup;
    }

    private AvailabilitySet getAvailabilitySet() {
        AvailabilitySet availabilitySet = azure.availabilitySets()
                .define("myAvailabilitySet")
                .withRegion(Region.EUROPE_WEST)
                .withNewResourceGroup(RESOURCE_GROUP)                //Creating new resource group here
                .withSku(AvailabilitySetSkuTypes.MANAGED)
                .create();
        return availabilitySet;
    }

    public void deleteVM(String VMName) {
        if (VMName.equals("LoadBalancer") || VMName.equals("rootVM")) {
            System.out.println("Deleting Load Balancer and rootVM not allowed!");
            return;
        }
        //@TODO check if VM exists?
        System.out.println("Starting to delete VM: " + VMName + "...");
        azure.virtualMachines().deleteByResourceGroup(RESOURCE_GROUP, VMName);
        System.out.println("Starting to delete virtual network...");
        azure.networkInterfaces().deleteByResourceGroup(RESOURCE_GROUP, VMName);
        System.out.println("Starting to delete public IP...");
        azure.publicIPAddresses().deleteByResourceGroup(RESOURCE_GROUP, VMName);
        System.out.println("Delete successful for VM: " + VMName + ".");
    }

    public List<VirtualMachine> getVirtualMachines() {
        return azure.virtualMachines().listByResourceGroup(RESOURCE_GROUP);
    }

    public List<String> getIPAddresses() {
        List<String> IPAddresses = new ArrayList<String>();
        for (VirtualMachine vm : getVirtualMachines()) {
            IPAddresses.add(vm.getPrimaryPublicIPAddress().ipAddress());
        }
        return IPAddresses;
    }

    public List<String> getNames() {
        List<String> names = new ArrayList<String>();
        for (VirtualMachine vm : getVirtualMachines()) {
            names.add(vm.name());
        }
        return names;
    }

    public List<String> getDeletableNames() {
        List<String> names = new ArrayList<String>();
        for (VirtualMachine vm : getVirtualMachines()) {
            if (!vm.name().equals("LoadBalancer") && !vm.name().equals("rootVM")) {
                names.add(vm.name());
            }
        }
        return names;
    }

    private void removeRandomVM() {
        if(getDeletableNames().size() > 0) {
          deleteVM(getDeletableNames().get(0));
        }
    }

    public void controlVirtualMachines(String monitorIP, String monitorPort) {
        System.out.println("Started controller...");
        boolean continueControlling = true;
        while(continueControlling) {
            try {
                URL oracle = new URL("http://" + monitorIP + ":" + monitorPort + "/txt/cpu-usage/current/rootVM");
                URLConnection yc = oracle.openConnection();
                BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
                String rootVMCPUPercentage = in.readLine();
                in.close();
                System.out.println("Root VM CPU percentage: " + rootVMCPUPercentage);
                raiseOrLowerVMCount(rootVMCPUPercentage);
                Thread.sleep(10_000L);
            } catch (Exception e) {
                continueControlling = false;
                Thread.currentThread().interrupt();
            }
        }
    }

    int lowSpike = 0;
    int highSpike = 0;
    private void raiseOrLowerVMCount(String rootVMCPUPercentage) {
        if(System.currentTimeMillis() - lastActionEpoch > 60000L) {
            System.out.println("Checking if VM count needs changing.");
            if (Double.parseDouble(rootVMCPUPercentage) > 80.0 && this.getVirtualMachines().size() < 4) {
                if (highSpike > 2) {
                    System.out.println("Raising VM count from " + this.getVirtualMachines().size() + " to " + (this.getVirtualMachines().size() + 1));
                    Random rand = new Random();
                    int  n = rand.nextInt(9999999) + 1;
                    createVM("VM-" + n);
                    lastActionEpoch = System.currentTimeMillis();
                    highSpike = 0;
                } else {
                    highSpike += 1;
                }
            } else {
                highSpike = 0;
            }
            if (Double.parseDouble(rootVMCPUPercentage) < 40.0 && this.getVirtualMachines().size() > 2) {
                if (lowSpike > 2) {
                    System.out.println("Lowering VM count from " + this.getVirtualMachines().size() + " to " + (this.getVirtualMachines().size() -1));
                    removeRandomVM();
                    lastActionEpoch = System.currentTimeMillis();
                    lowSpike = 0;
                } else {
                    lowSpike += 1;
                }

            } else {
                lowSpike = 0;
            }
        }
    }
}
