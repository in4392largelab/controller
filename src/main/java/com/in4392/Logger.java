package com.in4392;

import com.microsoft.azure.management.containerinstance.IpAddress;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Logger extends Thread {

    String monitorIp;
    String monitorPort;

    public Logger(String monitorIp, String monitorPort) {
        this.monitorIp = monitorIp;
        this.monitorPort = monitorPort;
    }

    @Override
    public void run() {
        String cpuPercentageUrl = "http://" + monitorIp + ":" + monitorPort + "/txt/cpu-usage/current/rootVM";
        String vmCountUrl = "http://" + monitorIp + ":" + monitorPort + "/txt/running-vms/num";

        while (true) {
            try {
                File file = new File("log.txt");
                FileWriter fr = null;
                fr = new FileWriter(file, true);
                BufferedWriter br = new BufferedWriter(fr);

                Date date = new Date();
                DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
                String strDate = dateFormat.format(date);

                br.write(strDate + "," + getStatsFromURL(cpuPercentageUrl) + "," + getStatsFromURL(vmCountUrl) + "\n");

                br.close();
                fr.close();

                Thread.sleep(10_000L);
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

    private String getStatsFromURL(String url) {
        try {
            URL oracle = new URL(url);
            URLConnection yc = oracle.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
            String response = in.readLine();
            in.close();
            return response;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "N/A";
    }
}
